import { WlsDashboardPage } from './app.po';

describe('wls-dashboard App', function() {
  let page: WlsDashboardPage;

  beforeEach(() => {
    page = new WlsDashboardPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
