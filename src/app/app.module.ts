import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material'

import { AppComponent } from './app.component';
import { ServerInfoComponent } from './components/server-info/server-info.component';

import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,
    ServerInfoComponent
  ],
  imports: [
    BrowserModule,
		BrowserAnimationsModule,
    FormsModule,
    HttpModule,
		MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
