import { Component, AfterViewInit, ElementRef } from '@angular/core';
import { MdCardModule } from '@angular/material';
import { UUID } from 'angular2-uuid';
import { WlsServerService } from '../../services/wls-server.service';
import { ServerInfoCall } from '../../classes/server-info-call';
import { ServerDetails } from '../../classes/server-details';

declare var CanvasJS: any;

@Component({
  selector: 'app-server-info',
  templateUrl: './server-info.component.html',
  styleUrls: ['./server-info.component.css'],
	providers: [WlsServerService]
})
export class ServerInfoComponent implements AfterViewInit {

	refreshRates = [
		{"millis": 5000, "text": "5 seconds", "axisXInterval": 10, "viewportMinimum": -1},
		{"millis": 15000, "text": "15 seconds", "axisXInterval": 15, "viewportMinimum": -1},
		{"millis": 30000, "text": "30 seconds", "axisXInterval": 30, "viewportMinimum": -5},
		{"millis": 60000, "text": "1 minute", "axisXInterval": 60, "viewportMinimum": -30},
		{"millis": 300000, "text": "5 minutes", "axisXInterval": 300, "viewportMinimum": -60}
	];
	selectedRefreshRate = this.refreshRates[0];
	
	onChangeRefreshRate(refreshRate) {
		this.setRefreshInterval(refreshRate.millis);
		console.log("onChangeRefreshRate:" + this.selectedRefreshRate + ", refreshRate:" + refreshRate);
	}
	
	serverInfoCall: ServerInfoCall;
	serverDetails: ServerDetails;
	
	chart: any;
	chartId: string;
	refreshInterval: any = 0;
	
	constructor(private wlsServerService: WlsServerService, elm: ElementRef) {
		this.chartId = "chart" + UUID.UUID();
		this.serverInfoCall = new ServerInfoCall(
			elm.nativeElement.getAttribute('host'),
			elm.nativeElement.getAttribute('port'),
			elm.nativeElement.getAttribute('username'),
			elm.nativeElement.getAttribute('password'));
	}
	
	ngAfterViewInit() {
			
    this.chart = new CanvasJS.Chart(this.chartId,
    {
      zoomEnabled: true,
      zoomType: "x",
      title:{
				text: "Heap Memory"
      },
			toolTip:{
				shared:true
			},
			axisX: {
				valueFormatString: "HH:mm:ss",
				interval: 15,
				intervalType: "second",
			},
			axisY: {
				valueFormatString: "#,###,,.##M",
				interlacedColor: "#F5F5F5",
				gridColor: "#D7D7D7",      
	 			tickColor: "#D7D7D7"
			},
			data: [
				{
					type: "splineArea",
					lineThickness: 1,
					showInLegend: true,
					name: "Free Memory",
					dataPoints: []
				},
				{
					type: "spline",
					lineThickness: 1,
					showInLegend: true,
					name: "Memory Size",
					dataPoints: []
				}
      ]
    });
		
		this.setRefreshInterval(this.selectedRefreshRate.millis);
		
	}
	
	private setRefreshInterval(millis: number) {
		//console.log("setRefreshInterval:" + millis);
		this.refreshData();
		if (this.refreshInterval != 0) {
			clearInterval(this.refreshInterval);
			this.refreshInterval = 0;
		}
		this.refreshInterval = setInterval(()=> {
       this.refreshData();
			}, millis);
	}
	
	private refreshData() {
	
		this.wlsServerService.getServerInfo(this.serverInfoCall).then(result => {
		
			this.serverDetails = result.item;

			var dt = new Date();
			this.chart.options.data[0].dataPoints.push({
				x: dt,
				y: result.item.heapFreeCurrent,
				markerSize: 4
			});
			this.chart.options.data[1].dataPoints.push({
				x: dt,
				y: result.item.heapSizeCurrent,
				markerSize: 4
			});
			
			this.chart.options.axisX.viewportMinimum = new Date(
				dt.getFullYear(), dt.getMonth(), dt.getDate(),
				dt.getHours(), dt.getMinutes() + this.selectedRefreshRate.viewportMinimum, dt.getSeconds());
			
			this.chart.options.axisX.interval = this.selectedRefreshRate.axisXInterval;
			//console.log("getServerInfo axisXInterval:" + this.selectedRefreshRate.axisXInterval);
			this.chart.render();
		});
	}
}

