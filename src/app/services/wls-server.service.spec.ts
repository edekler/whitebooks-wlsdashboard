/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WlsServerService } from './wls-server.service';

describe('WlsServerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WlsServerService]
    });
  });

  it('should ...', inject([WlsServerService], (service: WlsServerService) => {
    expect(service).toBeTruthy();
  }));
});
