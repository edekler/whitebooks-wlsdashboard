import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ResponseDefaultserver } from '../classes/response-defaultserver';
import { ServerInfoCall } from '../classes/server-info-call';

import 'rxjs/Rx';

@Injectable()
export class WlsServerService {

  constructor(private http: Http) {	}
	
	getServerInfo(serverInfoCall: ServerInfoCall): Promise<ResponseDefaultserver> {
		return this.http.get(serverInfoCall.host + ':' + serverInfoCall.port + '/management/wls/latest/servers/id/DefaultServer', {
			headers: new Headers({
				'Authorization': 'Basic ' + btoa(serverInfoCall.username + ':' + serverInfoCall.password),
				'Cache-Control': 'no-cache',
        'Pragma': 'no-cache',
        'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT'
			})
		})
		.map((res) => res.json() as ResponseDefaultserver).toPromise()
		.catch(this.handleError);;
	}
	
	private handleError(error: any): Promise<any> {
		console.error('An error occurred', error);
		return Promise.reject(error.message || error);
	}
}
