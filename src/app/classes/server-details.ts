import { ServerHealth } from './server-health';

export class ServerDetails {

  constructor(public name: string,
		public state: string,
    public heapFreeCurrent: number,
		public heapSizeCurrent: number,
    public health: ServerHealth,
		public usedPhysicalMemory: number,
		public activeHttpSessionCount: number,
		public activeThreadCount: number) {
	}
}
