export class ServerInfoCall {

  constructor(public host: string,
		public port: string,
    public username: string,
		public password: string) {
	}
}
